// dom elements
const form = document.getElementById('form');
const postsDOM = document.getElementById('posts');
const query = document.getElementById('search');
const loading = document.querySelector('.loader');

//variables
let initialPoint = 0;
let endPoint = 5;
let posts = null;
let postsPortion = null;

// Fetch posts from API
async function getPosts(){    
    const results = await fetch(`https://jsonplaceholder.typicode.com/posts`);
    const data = await results.json();
    return data;    
}


/*
Search query is an 'input' listener -> every letter entered/deleted peforms a search query

Ex - there is a word 'good morning' - which is the only instance on the page
As soon as someone writes:
 good -> all show
 good m -> only that 'good morning' shows
 */
function filterSearchQuery(event){
    
    //get search query
    const searchQuery = event.target.value.toLowerCase();      
    
    // check present posts for query
    const allPosts = document.querySelectorAll('.post');
    

    allPosts.forEach(post => {
        const postTitle = post.querySelector('.post-title').innerText.toLowerCase();
        
        const postBody = post.querySelector('.post-body').innerText.toLowerCase();

        if(postTitle.indexOf(searchQuery) > -1 || postBody.indexOf(searchQuery) > -1){
            
            post.style.display = 'flex';
        }
        else{
            post.style.display = 'none';
        }
    })   
}

//show loader, after 1 sec - remove loader, show posts
function showLoader(){

    //show loader
    loading.classList.add('show');

    //remove loader
    setTimeout(() => {
    loading.classList.remove('show');

    //show posts
    setTimeout(showPosts, 300);


    }, 1000);
}

// Add posts to DOM
async function showPosts()
{
    posts = await getPosts();
    
    postsPortion = posts.slice(initialPoint, endPoint);

    initialPoint += 5;
    endPoint += 5;


    postsPortion.forEach(post => {

        const postDiv = document.createElement('div');
        postDiv.classList.add('post');

        postDiv.innerHTML = `
        <div class="post-number">${post.id}</div>
        <div class="post-info">
            <h2 class="post-title">${post.title}</h2>
            <p class="post-body">${post.body}</p>
        </div>
        `;

        postsDOM.appendChild(postDiv);
    });

}


window.addEventListener('scroll', () => {

    //use destructuring to take specific elements from document.documentElement
    const {scrollTop, scrollHeight, clientHeight} = document.documentElement;

    //calculates if we reached the bottom of the list
    if(scrollTop + clientHeight >= scrollHeight){
        showLoader();        
    }
});

query.addEventListener('input', filterSearchQuery);


showPosts();
